package com.emiratesauction.cars.network;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class WebServiceClient {
    static HttpLoggingInterceptor interceptor;
    static OkHttpClient okHttpClient;
    private static Retrofit retrofit = null;


    public static Retrofit getClient() {
        interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS).addInterceptor(interceptor)
                .build();

        if (retrofit == null)

        {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Urls.END_POINT)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        return retrofit;

    }


}

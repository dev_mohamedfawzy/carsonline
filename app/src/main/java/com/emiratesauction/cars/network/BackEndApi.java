package com.emiratesauction.cars.network;

import com.emiratesauction.cars.model.car.Cars;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BackEndApi {


    @GET(Urls.CARS)
    Call<Cars> GET_CARS();

}


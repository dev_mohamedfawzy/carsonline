
package com.emiratesauction.cars.model.car;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cars {

    @SerializedName("alertEn")
    @Expose
    private String alertEn;
    @SerializedName("alertAr")
    @Expose
    private String alertAr;
    @SerializedName("Error")
    @Expose
    private Error error;
    @SerializedName("RefreshInterval")
    @Expose
    private int refreshInterval;
    @SerializedName("Ticks")
    @Expose
    private String ticks;
    @SerializedName("count")
    @Expose
    private int count;
    @SerializedName("endDate")
    @Expose
    private int endDate;
    @SerializedName("sortOption")
    @Expose
    private String sortOption;
    @SerializedName("sortDirection")
    @Expose
    private String sortDirection;
    @SerializedName("Cars")
    @Expose
    private List<Car> cars = null;

    public String getAlertEn() {
        return alertEn;
    }

    public void setAlertEn(String alertEn) {
        this.alertEn = alertEn;
    }

    public String getAlertAr() {
        return alertAr;
    }

    public void setAlertAr(String alertAr) {
        this.alertAr = alertAr;
    }

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public int getRefreshInterval() {
        return refreshInterval;
    }

    public void setRefreshInterval(int refreshInterval) {
        this.refreshInterval = refreshInterval;
    }

    public String getTicks() {
        return ticks;
    }

    public void setTicks(String ticks) {
        this.ticks = ticks;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getSortOption() {
        return sortOption;
    }

    public void setSortOption(String sortOption) {
        this.sortOption = sortOption;
    }

    public String getSortDirection() {
        return sortDirection;
    }

    public void setSortDirection(String sortDirection) {
        this.sortDirection = sortDirection;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

}

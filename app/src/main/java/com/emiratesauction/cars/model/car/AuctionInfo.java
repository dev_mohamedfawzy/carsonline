
package com.emiratesauction.cars.model.car;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuctionInfo {

    @SerializedName("bids")
    @Expose
    private int bids;
    @SerializedName("endDate")
    @Expose
    private int endDate;
    @SerializedName("endDateEn")
    @Expose
    private String endDateEn;
    @SerializedName("endDateAr")
    @Expose
    private String endDateAr;
    @SerializedName("currencyEn")
    @Expose
    private String currencyEn;
    @SerializedName("currencyAr")
    @Expose
    private String currencyAr;
    @SerializedName("currentPrice")
    @Expose
    private double currentPrice;
    @SerializedName("minIncrement")
    @Expose
    private int minIncrement;
    @SerializedName("lot")
    @Expose
    private int lot;
    @SerializedName("priority")
    @Expose
    private int priority;
    @SerializedName("VATPercent")
    @Expose
    private int vATPercent;
    @SerializedName("isModified")
    @Expose
    private int isModified;
    @SerializedName("itemid")
    @Expose
    private int itemid;
    @SerializedName("iCarId")
    @Expose
    private int iCarId;
    @SerializedName("iVinNumber")
    @Expose
    private String iVinNumber;

    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }

    public int getEndDate() {
        return endDate;
    }

    public void setEndDate(int endDate) {
        this.endDate = endDate;
    }

    public String getEndDateEn() {
        return endDateEn;
    }

    public void setEndDateEn(String endDateEn) {
        this.endDateEn = endDateEn;
    }

    public String getEndDateAr() {
        return endDateAr;
    }

    public void setEndDateAr(String endDateAr) {
        this.endDateAr = endDateAr;
    }

    public String getCurrencyEn() {
        return currencyEn;
    }

    public void setCurrencyEn(String currencyEn) {
        this.currencyEn = currencyEn;
    }

    public String getCurrencyAr() {
        return currencyAr;
    }

    public void setCurrencyAr(String currencyAr) {
        this.currencyAr = currencyAr;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getMinIncrement() {
        return minIncrement;
    }

    public void setMinIncrement(int minIncrement) {
        this.minIncrement = minIncrement;
    }

    public int getLot() {
        return lot;
    }

    public void setLot(int lot) {
        this.lot = lot;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getVATPercent() {
        return vATPercent;
    }

    public void setVATPercent(int vATPercent) {
        this.vATPercent = vATPercent;
    }

    public int getIsModified() {
        return isModified;
    }

    public void setIsModified(int isModified) {
        this.isModified = isModified;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    public int getICarId() {
        return iCarId;
    }

    public void setICarId(int iCarId) {
        this.iCarId = iCarId;
    }

    public String getIVinNumber() {
        return iVinNumber;
    }

    public void setIVinNumber(String iVinNumber) {
        this.iVinNumber = iVinNumber;
    }

}

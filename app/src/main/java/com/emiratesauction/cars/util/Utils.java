package com.emiratesauction.cars.util;

import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.emiratesauction.cars.R;

import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by mohamed on 01/01/17.
 */

public class Utils {


    /***
     * Display different between endDate and now
     * @param calendar
     * @return long mills
     */
    public static long getDiffernetTime(Calendar calendar) {
        long millis = calendar.getTimeInMillis() - Calendar.getInstance().getTimeInMillis();
        return millis;
    }


    /***
     * get Current Calendar From String Date
     * @param englishDate ex : 10 Jul 6:00 PM
     * @return Calendar object
     * @throws ParseException
     */

    public static Calendar getDateFromStr(String englishDate) throws ParseException {
        SimpleDateFormat sourceFormat = new SimpleDateFormat("dd MMM h:mm a", Locale.US);
        Date parsed = sourceFormat.parse(englishDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(parsed);
        calendar.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR));
        return calendar;
    }


    public static void NoConncetionAlert(View v) {

        Snackbar snackbar = Snackbar.make(v
                , R.string.no_connection, Snackbar.LENGTH_LONG);
        View view = snackbar.getView();
        TextView textView = (TextView) view.findViewById(android.support.design.R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER);
        textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cloud_off_white_24dp, 0, 0, 0);
        textView.setCompoundDrawablePadding(10);
        snackbar.show();
    }

    public static void timeOutAlert(View view) {

        Snackbar.make(view
                , R.string.time_out, Snackbar.LENGTH_LONG).show();
    }

    public static void Alert(View view, int msg) {

        Snackbar.make(view
                , msg, Snackbar.LENGTH_LONG).show();


    }

    public static void Alert(View view, String msg) {

        Snackbar.make(view
                , msg, Snackbar.LENGTH_LONG).show();


    }


    public static void errorAlert(View view) {


        Snackbar.make(view
                , "server error", Snackbar.LENGTH_SHORT).show();
    }

    public static void handleException(View view, Throwable t) {
        if (t instanceof SocketTimeoutException)
            timeOutAlert(view);
        else if (t instanceof UnknownHostException)
            NoConncetionAlert(view);

        else if (t instanceof ConnectException)
            NoConncetionAlert(view);
        else
            errorAlert(view);

    }


}

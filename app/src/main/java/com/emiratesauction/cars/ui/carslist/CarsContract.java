package com.emiratesauction.cars.ui.carslist;

import com.emiratesauction.cars.model.car.Car;
import com.emiratesauction.cars.ui.BaseView;

import java.util.List;

public interface CarsContract {

    interface IView extends BaseView {
        void setCars(List<Car> cars);

        void setRefreshInterval(int sec);

        void updateCars(List<Car> cars);

    }

    interface IPresenter {
        void getCars(boolean update);
    }
}

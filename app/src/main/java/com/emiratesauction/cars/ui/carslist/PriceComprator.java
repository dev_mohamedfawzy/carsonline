package com.emiratesauction.cars.ui.carslist;

import com.emiratesauction.cars.model.car.Car;

import java.util.Comparator;

public class PriceComprator implements Comparator<Car> {
    boolean highToLow;

    public PriceComprator(boolean highToLow) {
        this.highToLow = highToLow;
    }

    @Override
    public int compare(Car car, Car t1) {
        if (car.getAuctionInfo().getCurrentPrice() < t1.getAuctionInfo().getCurrentPrice()) {
            if (highToLow)
                return 1;
            else
                return -1;
        } else {
            if (highToLow)
                return -1;
            else
                return 1;
        }


    }
}

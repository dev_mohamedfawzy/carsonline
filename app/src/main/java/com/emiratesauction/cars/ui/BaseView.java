package com.emiratesauction.cars.ui;

/**
 * Created by mf4wzy on 17/06/17.
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void onFailure(Throwable t);


}

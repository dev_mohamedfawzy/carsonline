package com.emiratesauction.cars.ui.carslist;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.emiratesauction.cars.model.car.Car;

import java.util.Comparator;

public class DateComperator implements Comparator<Car> {


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int compare(Car car, Car t1) {
        return Integer.compare(car.getAuctionInfo().getEndDate(), t1.getAuctionInfo().getEndDate());
    }
}

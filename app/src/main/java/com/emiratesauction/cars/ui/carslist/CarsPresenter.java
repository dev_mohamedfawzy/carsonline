package com.emiratesauction.cars.ui.carslist;

import com.emiratesauction.cars.model.car.Cars;
import com.emiratesauction.cars.network.BackEndApi;
import com.emiratesauction.cars.network.WebServiceClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarsPresenter implements CarsContract.IPresenter, Callback<Cars> {
    CarsContract.IView view;
    boolean update;

    public CarsPresenter(CarsContract.IView view) {
        this.view = view;
    }

    @Override
    public void getCars(boolean update) {
        this.update = update;
        if (!update)
            view.showProgress();
        WebServiceClient.getClient().create(BackEndApi.class)
                .GET_CARS().enqueue(this);


    }

    @Override
    public void onResponse(Call<Cars> call, Response<Cars> response) {
        view.hideProgress();
        if (response.isSuccessful()) {
            if (update)
                view.updateCars(response.body().getCars());
            else
                view.setCars(response.body().getCars());
            view.setRefreshInterval(response.body().getRefreshInterval());
        }

    }

    @Override
    public void onFailure(Call<Cars> call, Throwable t) {
        view.hideProgress();
        view.onFailure(t);

    }
}

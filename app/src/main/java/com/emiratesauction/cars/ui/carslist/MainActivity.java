package com.emiratesauction.cars.ui.carslist;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;

import com.emiratesauction.cars.R;
import com.emiratesauction.cars.model.car.Car;
import com.emiratesauction.cars.util.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements CarsContract.IView {

    @BindView(R.id.recyclerCars)
    RecyclerView recyclerCars;
    @BindView(R.id.bottom_sheet)
    LinearLayout layoutBottomSheet;
    @BindView(R.id.swipeLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.contentLayout)
    CoordinatorLayout contentLayout;

    CarsPresenter carsPresenter;
    CarListAdapter adapter;
    CountDownTimer countDownTimer;
    BottomSheetBehavior sheetBehavior;
    List<Car> cars = new ArrayList<>();


    final int PRICE_LOW_TO_HIGH = 1, PRICE_HIGH_TO_LOW = 2, DATE_SORT = 3, YEAR_SORT = 4;
    int lastSort = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        carsPresenter = new CarsPresenter(this);
        adapter = new CarListAdapter(Locale.getDefault().getLanguage());
        recyclerCars.setLayoutManager(new LinearLayoutManager
                (this, LinearLayoutManager.VERTICAL, false));
        recyclerCars.setAdapter(adapter);
        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                carsPresenter = new CarsPresenter(MainActivity.this);
            }
        });


    }


    @Override
    public void setCars(List<Car> cars) {
        this.cars.clear();
        this.cars.addAll(cars);

        switch (lastSort) {
            case PRICE_HIGH_TO_LOW:
                priceHighToLow();
                break;
            case PRICE_LOW_TO_HIGH:
                priceLowToHigh();
                break;
            case DATE_SORT:
                sortByDate();
                break;
            case YEAR_SORT:
                sortByYear();
                break;
            default:
                adapter.addAll(cars);
                break;
        }

    }

    @Override
    public void setRefreshInterval(int sec) {
        //cancel the last one first
        if (countDownTimer != null)
            countDownTimer.cancel();
        countDownTimer = new CountDownTimer(sec * 1000, 1000) {
            @Override
            public void onTick(long l) {

            }

            @Override
            public void onFinish() {

                carsPresenter.getCars(true);
            }
        };
        countDownTimer.start();
    }

    @Override
    public void updateCars(List<Car> cars) {
        adapter.updateCars(cars);
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.setRefreshing(true);

    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onFailure(Throwable t) {
        Utils.handleException(contentLayout, t);

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (countDownTimer != null)
            countDownTimer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        carsPresenter.getCars(false);
    }

    public void toggleBottomSheet() {
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        } else {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    @OnClick(R.id.tv_sort)
    void sort() {
        toggleBottomSheet();
    }

    @OnClick(R.id.tv_sortDate)
    void sortByDate() {
        Collections.sort(cars, new DateComperator());
        adapter.addAll(cars);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        lastSort = DATE_SORT;

    }

    @OnClick(R.id.tv_priceLowToHigh)
    void priceLowToHigh() {

        Collections.sort(cars, new PriceComprator(false));
        adapter.addAll(cars);

        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        lastSort = PRICE_LOW_TO_HIGH;
    }

    @OnClick(R.id.tv_priceHighToLow)
    void priceHighToLow() {

        Collections.sort(cars, new PriceComprator(true));
        adapter.addAll(cars);

        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        lastSort = PRICE_HIGH_TO_LOW;
    }

    @OnClick(R.id.tv_year)
    void sortByYear() {
        Collections.sort(cars, new YearComeprator());
        adapter.addAll(cars);
        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        lastSort = YEAR_SORT;


    }
}

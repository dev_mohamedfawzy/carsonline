package com.emiratesauction.cars.ui.carslist;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emiratesauction.cars.R;
import com.emiratesauction.cars.model.car.Car;
import com.emiratesauction.cars.util.Utils;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.CarsViewHolder> {

    List<Car> cars;
    String language;

    public CarListAdapter(String language) {
        cars = new ArrayList<>();
        this.language = language;
    }

    @NonNull
    @Override
    public CarsViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View root = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.car_item, viewGroup, false);


        return new CarsViewHolder(root);
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder holder, int position) {
        Car car = cars.get(position);
        holder.iv_fav.setImageResource(car.isFav() ? R.drawable.ic_favorite : R.drawable.ic_unfavorite);
        String img = car.getImage().replace("[w]", "200")
                .replace("[h]", "200");
        Picasso.with(holder.itemView.getContext()).load(img)
                .into(holder.iv_carImg);
        String carName = (language.equals("ar") ? car.getModelAr() : car.getModelEn())
                + " " + car.getYear();
        holder.tv_carName.setText(carName);
        holder.tv_price.setText(String.valueOf(car.getAuctionInfo().getCurrentPrice()));
        holder.tv_currency.setText(language.equals("ar") ?
                car.getAuctionInfo().getCurrencyAr() : car.getAuctionInfo().getCurrencyEn());
        holder.tv_bids.setText(String.valueOf(car.getAuctionInfo().getBids()));
        holder.tv_lot.setText(String.valueOf(car.getAuctionInfo().getLot()));
        try {

            long timeInMills = Utils.getDiffernetTime(Utils.getDateFromStr(car.getAuctionInfo().getEndDateEn()));
            /*
            if time is null so it's expire date and should be removed from list
             */
            if (timeInMills > 0) {

                String time = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(timeInMills),
                        TimeUnit.MILLISECONDS.toMinutes(timeInMills) -
                                TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeInMills)),
                        TimeUnit.MILLISECONDS.toSeconds(timeInMills)
                                - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timeInMills)));
                holder.tv_time.setText(time);

                //if less than 5 mins
                if (timeInMills < (5 * 60 * 1000))
                    holder.tv_time.setTextColor(Color.RED);
                else holder.tv_time.setTextColor(Color.BLACK);
            } else {
                cars.remove(car);
                notifyDataSetChanged();
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    void addAll(List<Car> cars) {
        this.cars.clear();
        this.cars.addAll(cars);
        notifyDataSetChanged();

    }


    @Override
    public int getItemCount() {
        return cars.size();
    }

    public void updateCars(List<Car> cars) {
        for (Car car : cars) {
            if (this.cars.contains(car)) {
                int index = this.cars.indexOf(car);
                this.cars.get(index).getAuctionInfo().setEndDate(car.getAuctionInfo().getEndDate());
                this.cars.get(index).getAuctionInfo().setEndDateEn(car.getAuctionInfo().getEndDateEn());
                this.cars.get(index).getAuctionInfo().setEndDateAr(car.getAuctionInfo().getEndDateAr());
                notifyItemChanged(index);
            }
        }

    }

    class CarsViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_carImg)
        ImageView iv_carImg;
        @BindView(R.id.iv_fav)
        ImageView iv_fav;

        @BindView(R.id.tv_carName)
        TextView tv_carName;
        @BindView(R.id.tv_price)
        TextView tv_price;
        @BindView(R.id.tv_currency)
        TextView tv_currency;
        @BindView(R.id.tv_lot)
        TextView tv_lot;
        @BindView(R.id.tv_bids)
        TextView tv_bids;
        @BindView(R.id.tv_time)
        TextView tv_time;


        public CarsViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.iv_fav)
        void fav() {
            cars.get(getAdapterPosition()).setFav(!cars.get(getAdapterPosition()).isFav());
            notifyDataSetChanged();
        }
    }
}

# CarsOnline

sample app that displays information about Cars Online.
## Getting Started

These instructions will get you a copy of the project up and running on your local device for development and testing purposes
. See notes on how to see test cases and deploy the project.

### Prerequisites

 you need to be familiar with send http requests & dealing with apis.
 you need to be familiar with MVP design pattern.

### Architecture

Using MVP Design pattern cause it's a small scale project and MVP is very suitable for it.

why using MVP:
1-separate the code for the view, which we show to the user like recycler from the business logic which is triggered on user interaction with the view which we call presenter. The data that we show in the view should be provided by another separate module which we call as model. In turn code becomes readable, understandable and maintainable.
2-The communication between the view and the model needs to take place through the presenter i.e. The view and model cannot have reference of one another.
3-Using this design pattern we should be able to make changes in one components without having the need to change any code in the other two components. Also in case we want to completely replace the view or model with another implementation, that should be possible as well.
4-Maximize the code that can be tested with automation cause it's separate methods with presenter.

* reference : https://en.wikipedia.org/wiki/Model-view-presenter.


### Libraries

* Retrofit with Gson converter (send http requests)
http://square.github.io/retrofit/

* ButterKnife (injection views)
http://jakewharton.github.io/butterknife/

### Note
using gradle:3.1.3 .

# Finshed The Plus Point About Sorting.


